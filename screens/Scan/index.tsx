import React, {useRef, useState, useCallback} from 'react';
import {StyleSheet, Vibration} from 'react-native';
import {GoogleVisionBarcodesDetectedEvent, RNCamera} from 'react-native-camera';
import {SafeAreaView} from 'react-native-safe-area-context';
import BarcodeMask from 'react-native-barcode-mask';
import {useNavigation, useFocusEffect} from '@react-navigation/native';

export default () => {
  const camera = useRef(null);
  const [qrCodeDetected, setQrCodeDetected] = useState(false);
  const navigation = useNavigation();

  useFocusEffect(
    useCallback(() => {
      return () => {
        setQrCodeDetected(false);
      };
    }, []),
  );

  const onGoogleVisionBarcodesDetected = ({
    barcodes,
  }: GoogleVisionBarcodesDetectedEvent) => {
    if (qrCodeDetected) {
      Vibration.cancel();
      return;
    }
    if (barcodes.length) {
      setQrCodeDetected(true);
      Vibration.vibrate();
      return navigation.navigate('Home', {data: barcodes[0].data});
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <RNCamera
        ref={camera}
        style={styles.viewfinder}
        type={RNCamera.Constants.Type.back}
        captureAudio={false}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        onGoogleVisionBarcodesDetected={onGoogleVisionBarcodesDetected}>
        <BarcodeMask
          width={240}
          useNativeDriver={true}
          height={240}
          edgeColor={'#238CFD'}
          lineAnimationDuration={1000}
        />
      </RNCamera>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  viewfinder: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});
