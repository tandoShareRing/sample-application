import {useNavigation, useFocusEffect} from '@react-navigation/native';
import React, {useState, useEffect, useCallback} from 'react';
import {
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {createSign, createVerify} from 'shr-offline-data';
import Icon from 'react-native-vector-icons/Feather';

const Buffer = global.Buffer;

export default ({route}: any) => {
  const navigation = useNavigation();
  const [displayData, setDisplayData] = useState('');
  const [schemaValid, setSchemaValid] = useState(false);
  const [signatureValid, setSignatureValid] = useState(false);
  const [documentProofValid, setDocumentProofValid] = useState(false);
  const [twoDoses, setTwoDoses] = useState(false);
  const [twoDoses14d, setTwoDoses14d] = useState(false);

  useFocusEffect(
    useCallback(() => {
      return () => {
        setDisplayData('');
        setSchemaValid(false);
        setSignatureValid(false);
        setDocumentProofValid(false);
        setTwoDoses(false);
        setTwoDoses14d(false);
      };
    }, []),
  );

  useEffect(() => {

    console.log(new Date());
    const privateKey = "a15c6a39550f5e3ce621186a85632011630691094b8e6afb520ec1fc9390b7cc";
    const publicKey = "02728ab7eeac54e8e2039fbef0d1221bdb4cdd303b6a8f3730e6104ed9ee0a5b83";
    const data = "HKSARG|VAC|3.1|1|TC21819-5687585-7|****710(2)|LUNG, K* M** C*****|29-07-2021|Comirnaty COVID-19 mRNA Vaccine (BNT162b2) Concentrate for Dispersion for Injection|2019冠狀病毒病疫苗 (復必泰)|||19-08-2021|Comirnaty COVID-19 mRNA Vaccine (BNT162b2) Concentrate for Dispersion for Injection|2019冠狀病毒病疫苗 (復必泰)|||19-08-2021 18:50|HKSARG|VAC|3|1|TC21504-1309554-6|****642(8)|NATALE, T******|13-04-2021|Comirnaty COVID-19 mRNA Vaccine (BNT162b2) Concentrate for Dispersion for Injection|2019冠狀病毒病疫苗 (復必泰)|||04-05-2021|Comirnaty COVID-19 mRNA Vaccine (BNT162b2) Concentrate for Dispersion for Injection|2019冠狀病毒病疫苗 (復必泰)|||04-05-2021 11:59|HKSARG|VAC|3|1|TC21421-973435-0|****100(A)|CHAN, T*** Y*|24-03-2021|CoronaVac COVID-19 Vaccine (Vero Cell), Inactivated|2019冠狀病毒病疫苗 (克爾來福)|||21-04-2021|CoronaVac COVID-19 Vaccine (Vero Cell), Inactivated|2019冠狀病毒病疫苗 (克爾來福)|||21-04-2021 14:46";
    const delimiter = "|";
    const sign = createSign({privateKey, delimiter});
    const signed = sign.update(data).digest();
    const verify = createVerify({publicKey, signature: signed.signature, delimiter});
    const output = verify.update(data).digest();
    console.log(output);
    console.log(new Date());

  }, []);

  // useEffect(() => {
  //   const delimiter = '|';
  //   const time = 2 * 60; // seconds

  //   if (!route.params?.data) {
  //     return;
  //   }
  //   const arr = route.params.data.split(delimiter);

  //   if (arr[0] !== 'HKRU001') {
  //     setDisplayData(route.params.data);
  //     return;
  //   }

  //   setSchemaValid(true);

  //   Promise.all([arr[7], arr[arr.length - 3]].map(proof => getProof(proof)))
  //     .then(res => {
  //       if (res.some(r => !r || !r.proof)) {
  //         throw res;
  //       }
  //       setDocumentProofValid(true);
  //     })
  //     .catch(err => {
  //       console.log(err);
  //       setDocumentProofValid(false);
  //     });

  //   const publicKey = arr[arr.length - 2];
  //   const signature = arr[arr.length - 1];

  //   arr.pop();
  //   const dataToVerify = arr.join(delimiter);

  //   const verify = createVerify({
  //     publicKey,
  //     delimiter,
  //     signature,
  //     expiration: {enabled: true, time},
  //   });
  //   const verified = verify.update(dataToVerify).digest();

  //   setSignatureValid(verified.valid);
  //   setDisplayData(arr.join('\n'));

  //   const doses = [arr[10]];

  //   if (arr.length > 17) {
  //     doses.push(arr[15]);
  //   }

  //   if (doses.length > 1) {
  //     setTwoDoses(true);
  //     const [y, m, d] = doses[1].split('-');
  //     const diff = Math.floor(
  //       (Date.now() - new Date(y, m - 1, d).getTime()) / 1000 / 60 / 60 / 24,
  //     );
  //     if (diff >= 14) {
  //       setTwoDoses14d(true);
  //     }
  //   }
  // }, [route.params?.data]);

  const iconSuccess = () => <Icon name="check" color="green" size={16} />;
  const iconDanger = () => <Icon name="x" color="red" size={16} />;

  const rpcUrl = 'https://tencent.blockchain.testnet.sharetoken.io:26658/';

  const getProof = (proof: string) => {
    return fetch(rpcUrl, {
      method: 'POST',
      body: JSON.stringify({
        jsonrpc: '2.0',
        id: 1,
        method: 'abci_query',
        params: {
          path: 'custom/doc/proof',
          data: Buffer.from(JSON.stringify({Proof: proof}), 'utf-8').toString(
            'hex',
          ),
        },
      }),
    })
      .then(res => res.json())
      .then(res => {
        if (!res.result.response) {
          throw res;
        }
        res = res.result.response;
        if (res.code !== 0) {
          throw res;
        }
        res = Buffer.from(res.value, 'base64').toString('utf-8');
        return JSON.parse(res);
      })
      .catch(err => {
        console.error(err);
        return null;
      });
  };

  return (
    <SafeAreaView>
      <ScrollView
        automaticallyAdjustContentInsets={true}
        contentContainerStyle={styles.rows}>
        {!!displayData && (
          <View style={styles.row}>
            <Text style={styles.header}>Raw data</Text>
            <Text style={styles.code}>{displayData}</Text>
            <Text style={styles.header}>Result</Text>
            <Text style={styles.text}>
              Schema valid? {schemaValid ? iconSuccess() : iconDanger()}
            </Text>
            <Text style={styles.text}>
              Signature valid? {signatureValid ? iconSuccess() : iconDanger()}
            </Text>
            <Text style={styles.text}>
              Document proof valid?{' '}
              {documentProofValid ? iconSuccess() : iconDanger()}
            </Text>
            <Text style={styles.text}>
              Got 2 doses? {twoDoses ? iconSuccess() : iconDanger()}
            </Text>
            <Text style={styles.text}>
              At least 14 days after 2nd dose?{' '}
              {twoDoses14d ? iconSuccess() : iconDanger()}
            </Text>
          </View>
        )}
        <View style={styles.row}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Scan')}
            style={styles.scanButton}>
            <Text style={styles.scanButtonText}>Scan QR</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  rows: {
    flexDirection: 'column',
    alignItems: 'stretch',
    minHeight: Dimensions.get('window').height,
  },
  row: {
    padding: 20,
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    fontSize: 24,
    marginVertical: 15,
  },
  scanButton: {
    flex: 0,
    color: '#fff',
    backgroundColor: '#238CFD',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
  },
  scanButtonText: {
    textAlign: 'center',
    fontSize: 16,
    color: '#fff',
  },
  text: {
    fontSize: 16,
  },
  code: {
    fontSize: 16,
    fontFamily: 'Courier New, monospace',
  },
});
